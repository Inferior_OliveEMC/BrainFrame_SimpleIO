#include <math.h>
#include <stdio.h>
#include <stdlib.h>

//#include "Maxfiles.h"
//#include "infoli1.h"
#include "infoli0.h" 
//#include "infoli1.max"
#include "infoli0.max" 
#include "MaxSLiCInterface.h"


#define SIMTIME 20 //in ms for when no input file is provided
//Change the Time Mux Factor to change the number of simulated cells in the network
////////------Keep Netwotk size in multiples of 96 cells for channel alignment----////////
#define TIME_MUX_FACTOR 7680 //ATTENTION: per DFE

///----DO NOT TOUCH defines after this point--///
#define HW_CELLS 1 //per DFE
//Number of parameters for states
#define INI_PARAMETERS 24
//Maximum Number of cells and time-multiplexing
#define MAX_N_SIZE 7680 //Make sure this has the same value in the kernel code (NetMaxSize) per DFE
//#define MAX_TIME_MUX 10
#define DELTA 0.05
#define CONDUCTANCE 0.004
//IO network size is IO_NETWORK_DIM1*IO_NETWORK_DIM2 times the TIME_MUX_FACTOR
#define IO_NETWORK_SIZE HW_CELLS*TIME_MUX_FACTOR // !!!Per DFE!!!
#define UNROLL_FACTOR 16 // Make sure it has the same value as in the manager and kernel

typedef unsigned long long timestamp;
//Time stamp function for measuring DFE runtime
static timestamp getTimestamp(){

	struct timeval now;
	gettimeofday(&now, NULL);
	return now.tv_usec + (timestamp)now.tv_sec * 1000000;

}


int main(void)
{

    char *outFileName = "InferiorOlive_Output.txt";
    FILE *pOutFile;
    char temp[100];//warning: this buffer may overflow
    long simSteps = 0;
    long simTime = 0;
    //Timing variables
    timestamp t0,t1;
    float V,f, IC;
    float Facc = 0.0;
    float Vacc = 0.0;
    const long numEngines = 2; //Make sure this is correct for the DFE setup used!


    printf("Begin execution \n") ;
	const int IniVectorSize0 = INI_PARAMETERS* IO_NETWORK_SIZE;
	const int IniVectorSize1 = INI_PARAMETERS* IO_NETWORK_SIZE;


	int sizeBytesFloat = sizeof(float);
	float *IniVector0 = malloc(sizeBytesFloat * IniVectorSize0);
	float *IniICVector0 = malloc(sizeBytesFloat * IO_NETWORK_SIZE);

	float *IniVector1 = malloc(sizeBytesFloat * IniVectorSize1);
	float *IniICVector1 = malloc(sizeBytesFloat * IO_NETWORK_SIZE);


	
	long TimeMuxFactor = TIME_MUX_FACTOR;
	long MaxNSize = MAX_N_SIZE;
	long NetSize = IO_NETWORK_SIZE;
	
	//long MaxTimeMux = MAX_TIME_MUX;


// Cell Parameters Initialization
	float initOneCell[] = {
	/*0*/		-60, //V_dend
	/*1*/		0.0112788,// High-threshold calcium_r
	/*2*/		0.0049291,// Calcium-dependent potassium_s
	/*3*/		0.0337836,// H current_q
	/*4*/		3.7152,// Calcium concentration Ca2Plus
	/*5*/		0.5,// High-threshold calcium current I_CaH
				//Initial somatic parameters
	/*6*/		0.68, //default arbitrary value but it should be randomized per cell g_CaL
	/*7*/		-60, // V_soma
	/*8*/		1.0127807,// Sodium (artificial) Sodium_m
	/*9*/		0.3596066, //Sodium_h
	/*10*/		0.2369847,// Potassium (delayed rectifier) Potassium_n
	/*11*/		0.2369847, // Potassium_p
	/*12*/    	0.1,// Potassium (voltage-dependent) Potassium_x_s
	/*13*/		0.7423159,// Low-threshold calcium Calcium_k
	/*14*/		0.0321349,// Calcium_l
				// Initial axonal parameters
	/*15*/		-60, // v_Axon
				//sisaza: Sodium_m_a doesn't have a state, therefore this assignment doesn'thave any effect
	/*16*/		0.003596066,// Sodium (thalamocortical)  // Sodium_m_a
	/*17*/		0.9,// Sodium_h_a
	/*18*/		0.2369847,// Potassium (transient) // Potassium_x_a
	//filler for channel alignment
				0,
				0,
				0,
				0,
				0

			 };

//Create initial state input for all cells
	printf("Creating State Initialization Vector\n");
	for(long i = 0; i< IniVectorSize0 ; i=i+INI_PARAMETERS){
		for(long j = 0; j<INI_PARAMETERS; j++){
			IniVector0[i+j] = initOneCell[j];
			//printf(" Ini Vector : %ld \n", i+j);
		}
	}


	printf("Creating State Initialization Vector\n");
	for(long i = 0; i< IniVectorSize1 ; i=i+INI_PARAMETERS){
		for(long j = 0; j<INI_PARAMETERS; j++){
			IniVector1[i+j] = initOneCell[j];
			//printf(" Ini Vector : %ld \n", i+j);
		}
	}


//Create Initial ICs (Gap Junction Influences)
	printf("Creating Initialization Gap Junction Vector\n");
	for(long k = 0; k< IO_NETWORK_SIZE ; k++){
		for(long l = 0; l< IO_NETWORK_SIZE ; l++){
			V = IniVector0[k*24] - IniVector0[l*24];
			f = V * exp(-0.01 * V * V);
			Facc = f + Facc;
			Vacc = V + Vacc;
		}
		IC = CONDUCTANCE * (8 * Facc + 2 * Vacc);
		IniICVector0[k] = IC;		//printf("%ld : %.2f ",k, IniICVector[k]);
	}
	//printf("\n ");


//Create Initial ICs (Gap Junction Influences)
	printf("Creating Initialization Gap Junction Vector\n");
	for(long k = 0; k< IO_NETWORK_SIZE ; k++){
		for(long l = 0; l< IO_NETWORK_SIZE ; l++){
			V = IniVector1[k*24] - IniVector1[l*24];
			f = V * exp(-0.01 * V * V);
			Facc = f + Facc;
			Vacc = V + Vacc;
		}
		IC = CONDUCTANCE * (8 * Facc + 2 * Vacc);
		IniICVector1[k] = IC;		//printf("%ld : %.2f ",k, IniICVector[k]);
	}
	//printf("\n ");



//Create output file
    pOutFile = fopen(outFileName,"w");
    if(pOutFile==NULL){
        printf("Error: Couldn't create %s\n", outFileName);
        exit(EXIT_FAILURE);
    }
    sprintf(temp, "#simSteps iApp #Neuron Output(V_axon)\n");
    fputs(temp, pOutFile);


    simTime = SIMTIME; // in miliseconds
    simSteps = ceil(simTime/DELTA);
    
//Malloc evoked input memory
    float *iApp0 = malloc(sizeBytesFloat * simSteps * IO_NETWORK_SIZE);
    float *iApp1 = malloc(sizeBytesFloat * simSteps * IO_NETWORK_SIZE);
//Initialise iApp trace
    printf("Creating Input Trace\n");

	for (long i = 0 ; i < simSteps * IO_NETWORK_SIZE ; i++){
		if (i > ((20000-1) * IO_NETWORK_SIZE) + IO_NETWORK_SIZE-1 && i < (20500-1) * IO_NETWORK_SIZE){
			iApp0[i]= 6.0;
		}
		else{
			iApp0[i]= 0.0;
		}
	}

	for (long i = 0 ; i < simSteps * IO_NETWORK_SIZE ; i++){
		if (i > ((20000-1) * IO_NETWORK_SIZE) + IO_NETWORK_SIZE-1 && i < (20500-1) * IO_NETWORK_SIZE){
			iApp1[i]= 6.0;
		}
		else{
			iApp1[i]= 0.0;
		}
	}


//Malloc output memory
    long sizeBytesFloatOut = simSteps*IO_NETWORK_SIZE* sizeBytesFloat;
    printf("Mallocing Output Space\n");
    float *Out0 = malloc(sizeBytesFloatOut);
    float *Out1 = malloc(sizeBytesFloatOut);

//Kernel actions and execution

	max_file_t *maxfile0 =  infoli0_init();
	max_file_t *maxfile1 =  infoli0_init();
	max_file_t *maxfiles[] = {
		maxfile0,
		maxfile1
	};	


	if (maxfile0 == NULL)
		printf("Maxfile 0 Error.\n");

	if (maxfile1 == NULL)
		printf("Maxfile 1 Error.\n");

	if (maxfiles == NULL)
		printf("Maxfiles Error.\n");



	printf("Kernel Actions Init.\n");
	max_actions_t *actions0 = max_actions_init(maxfile0, NULL);
	max_actions_t *actions1 = max_actions_init(maxfile1, NULL);

	printf("Actions Init.\n");
	max_actarray_t *actArray = max_mixed_actarray_init(maxfiles,numEngines);

	printf("Loading engine array\n\n");
	max_engarray_t *engines = max_load_mixed_array(maxfiles, numEngines, "*");

	if (engines == NULL)
		printf("Engine Error.\n");

	printf("Defining Actions for Writing Initialization State to LMem.\n");
	max_queue_input(actions0, "fromcpu", IniVector0, INI_PARAMETERS * IO_NETWORK_SIZE * sizeBytesFloat);
 	max_lmem_linear(actions0, "cpu2lmem", 0, INI_PARAMETERS * IO_NETWORK_SIZE * sizeBytesFloat);
	max_ignore_kernel(actions0, "infoli0Kernel");
	max_ignore_lmem(actions0, "lmem2cpu");
	max_ignore_lmem(actions0, "Ini0");
	max_ignore_lmem(actions0, "iApp0");
	max_ignore_lmem(actions0, "IC0");
	max_ignore_lmem(actions0, "s0");


	
	max_queue_input(actions1, "fromcpu", IniVector1, INI_PARAMETERS * IO_NETWORK_SIZE * sizeBytesFloat);
 	max_lmem_linear(actions1, "cpu2lmem", 0, INI_PARAMETERS * IO_NETWORK_SIZE * sizeBytesFloat);
	max_ignore_kernel(actions1, "infoli0Kernel");
	max_ignore_lmem(actions1, "lmem2cpu");
	max_ignore_lmem(actions1, "Ini0");
	max_ignore_lmem(actions1, "iApp0");
	max_ignore_lmem(actions1, "IC0");
	max_ignore_lmem(actions1, "s0");


	printf("Setting array Actions.\n");


	max_push_action(actArray, actions0);
	max_push_action(actArray, actions1);

	if (actArray == NULL)
		printf("Action array pointer Error.\n");


	printf("Running on DFE.\n\n");
	max_run_array(engines, actArray);


	actions0 = max_actions_init(maxfile0, NULL);
	printf("Defining Actions for  Writing Input Trace State to LMem .\n");
	max_queue_input(actions0, "fromcpu", iApp0, simSteps*IO_NETWORK_SIZE * sizeBytesFloat);
 	max_lmem_linear(actions0, "cpu2lmem",(INI_PARAMETERS * (MaxNSize))*sizeBytesFloat, simSteps*IO_NETWORK_SIZE * sizeBytesFloat);
	max_ignore_kernel(actions0, "infoli0Kernel");
	max_ignore_lmem(actions0, "lmem2cpu");
	max_ignore_lmem(actions0, "Ini0");
	max_ignore_lmem(actions0, "iApp0");
	max_ignore_lmem(actions0, "IC0");
	max_ignore_lmem(actions0, "s0");

	actions1 = max_actions_init(maxfile1, NULL);
	max_queue_input(actions1, "fromcpu", iApp1, simSteps*IO_NETWORK_SIZE * sizeBytesFloat);
 	max_lmem_linear(actions1, "cpu2lmem",(INI_PARAMETERS * (MaxNSize))*sizeBytesFloat, simSteps*IO_NETWORK_SIZE * sizeBytesFloat);
	max_ignore_kernel(actions1, "infoli0Kernel");
	max_ignore_lmem(actions1, "lmem2cpu");
	max_ignore_lmem(actions1, "Ini0");
	max_ignore_lmem(actions1, "iApp0");
	max_ignore_lmem(actions1, "IC0");
	max_ignore_lmem(actions1, "s0");

	printf("Actions Init.\n");
	actArray = max_mixed_actarray_init(maxfiles,numEngines);
	max_push_action(actArray, actions0);
	max_push_action(actArray, actions1);

	if (actArray == NULL)
		printf("Action array pointer Error.\n");


	printf("Running on DFE.\n\n");
	max_run_array(engines, actArray);

	actions0 = max_actions_init(maxfile0, NULL);
	printf("Defining Actions for Writing IC Initialization Trace State to LMem.\n");
	max_queue_input(actions0, "fromcpu", IniICVector0, IO_NETWORK_SIZE * sizeBytesFloat);
 	max_lmem_linear(actions0, "cpu2lmem", ((INI_PARAMETERS * (MaxNSize))+ (simSteps*(MaxNSize)+(simSteps * (MaxNSize))))*sizeBytesFloat, IO_NETWORK_SIZE * sizeBytesFloat);
	max_ignore_kernel(actions0, "infoli0Kernel");
	max_ignore_lmem(actions0, "lmem2cpu");
	max_ignore_lmem(actions0, "Ini0");
	max_ignore_lmem(actions0, "iApp0");
	max_ignore_lmem(actions0, "IC0");
	max_ignore_lmem(actions0, "s0");

	actions1 = max_actions_init(maxfile1, NULL);
	max_queue_input(actions1, "fromcpu", IniICVector1, IO_NETWORK_SIZE * sizeBytesFloat);
 	max_lmem_linear(actions1, "cpu2lmem", ((INI_PARAMETERS * (MaxNSize))+ (simSteps*(MaxNSize)+(simSteps * (MaxNSize))))*sizeBytesFloat, IO_NETWORK_SIZE * sizeBytesFloat);
	max_ignore_kernel(actions1, "infoli0Kernel");
	max_ignore_lmem(actions1, "lmem2cpu");
	max_ignore_lmem(actions1, "Ini0");
	max_ignore_lmem(actions1, "iApp0");
	max_ignore_lmem(actions1, "IC0");
	max_ignore_lmem(actions1, "s0");


	printf("Actions Init.\n");
	actArray = max_mixed_actarray_init(maxfiles,numEngines);
	max_push_action(actArray, actions0);
	max_push_action(actArray, actions1);

	if (actArray == NULL)
		printf("Action array pointer Error.\n");


	printf("Running on DFE.\n\n");
	max_run_array(engines, actArray);
//-------------------------------------------------------------------------------------------------------------------------------------//

	actions0 = max_actions_init(maxfile0, NULL);
	actions1 = max_actions_init(maxfile1, NULL);

	printf("Setting DFE 0 Ticks.\n");

	uint64_t longOffset = max_get_offset_auto_loop_size(actions0, "infoli0Kernel", "writeLoop0");
	uint64_t ticks = (NetSize*(NetSize*numEngines)*simSteps)/UNROLL_FACTOR + (simSteps*longOffset);
	max_set_ticks(actions0, "infoli0Kernel", ticks);
	printf("Offset = %ld\n", longOffset);
	

	printf("Setting Scalars DFE 0.\n");

	max_set_double(actions0, "infoli0Kernel", "N_size", NetSize);
	max_set_double(actions0,"infoli0Kernel", "simsteps", simSteps);
	max_set_double(actions0,"infoli0Kernel", "time_mux_factor", TimeMuxFactor);
	max_set_double(actions0,"infoli0Kernel", "Max_N_Size", MaxNSize);
	max_set_double(actions0, "infoli0Kernel","DFENo", numEngines);
	
	max_ignore_lmem(actions0, "cpu2lmem");
	max_ignore_lmem(actions0, "lmem2cpu");
	

	max_lmem_linear(actions0, "Ini0", 0, INI_PARAMETERS * IO_NETWORK_SIZE * sizeBytesFloat);
	max_lmem_linear(actions0, "iApp0", (INI_PARAMETERS * (MaxNSize))*sizeBytesFloat, simSteps*IO_NETWORK_SIZE * sizeBytesFloat);
	max_lmem_linear(actions0, "IC0", ((INI_PARAMETERS * (MaxNSize))+ (simSteps*(MaxNSize)+(simSteps * (MaxNSize))))*sizeBytesFloat, IO_NETWORK_SIZE * sizeBytesFloat);
	max_lmem_linear(actions0, "s0", ((INI_PARAMETERS * MaxNSize)+ (simSteps*MaxNSize))*sizeBytesFloat, simSteps * IO_NETWORK_SIZE * sizeBytesFloat);



	printf("Setting DFE 1 Ticks.\n");
	longOffset = max_get_offset_auto_loop_size(actions1, "infoli0Kernel", "writeLoop0");
	ticks = (NetSize*(NetSize*numEngines)*simSteps)/UNROLL_FACTOR + (simSteps*longOffset);
	max_set_ticks(actions1, "infoli0Kernel", ticks);

	printf("Setting Scalars DFE 1.\n");

	max_set_double(actions1,"infoli0Kernel", "N_size", NetSize);
	max_set_double(actions1,"infoli0Kernel", "simsteps", simSteps);
	max_set_double(actions1,"infoli0Kernel","time_mux_factor", TimeMuxFactor);
	max_set_double(actions1,"infoli0Kernel", "Max_N_Size", MaxNSize);
	max_set_double(actions1, "infoli0Kernel","DFENo", numEngines);

	max_ignore_lmem(actions1, "cpu2lmem");
	max_ignore_lmem(actions1, "lmem2cpu");
	


	max_lmem_linear(actions1, "Ini0", 0, INI_PARAMETERS * IO_NETWORK_SIZE * sizeBytesFloat);
	max_lmem_linear(actions1, "iApp0", (INI_PARAMETERS * (MaxNSize))*sizeBytesFloat, simSteps*IO_NETWORK_SIZE * sizeBytesFloat);
	max_lmem_linear(actions1, "IC0", ((INI_PARAMETERS * (MaxNSize))+ (simSteps*(MaxNSize)+(simSteps * (MaxNSize))))*sizeBytesFloat, IO_NETWORK_SIZE * sizeBytesFloat);
	max_lmem_linear(actions1, "s0", ((INI_PARAMETERS * MaxNSize)+ (simSteps*MaxNSize))*sizeBytesFloat, simSteps * IO_NETWORK_SIZE * sizeBytesFloat);


	printf("Setting array Actions.\n");

	actArray = max_mixed_actarray_init(maxfiles,numEngines);
	max_push_action(actArray, actions0);
	max_push_action(actArray, actions1);

	if (actArray == NULL)
		printf("Action array pointer Error.\n");


	


	printf("Running Neuron Network on DFEs. Net Size = %ld and SimSteps = %ld\n", NetSize*numEngines, simSteps);
	t0 = getTimestamp();


	max_run_array(engines, actArray);

	t1 = getTimestamp();

	printf("DFE Runtime = %9.7f seconds\n\n", (t1-t0)/1000000.0 );


	actions0 = max_actions_init(maxfile0, NULL);
	printf("Setting Output Read phase from DFE 0.\n");	
 	max_lmem_linear(actions0, "lmem2cpu", ((INI_PARAMETERS * MaxNSize)+ (simSteps*MaxNSize))*sizeBytesFloat, simSteps * IO_NETWORK_SIZE * sizeBytesFloat);
	max_queue_output(actions0, "tocpu", Out0,simSteps * IO_NETWORK_SIZE *sizeBytesFloat);
	max_ignore_kernel(actions0, "infoli0Kernel");
	max_ignore_lmem(actions0, "cpu2lmem");
	max_ignore_lmem(actions0, "Ini0");
	max_ignore_lmem(actions0, "iApp0");
	max_ignore_lmem(actions0, "IC0");
	max_ignore_lmem(actions0, "s0");


	actions1 = max_actions_init(maxfile1, NULL);
	printf("Setting Output Read phase from DFE 1.\n");
 	max_lmem_linear(actions1, "lmem2cpu",((INI_PARAMETERS * MaxNSize)+ (simSteps*MaxNSize))*sizeBytesFloat, simSteps * IO_NETWORK_SIZE * sizeBytesFloat);
	max_queue_output(actions1, "tocpu", Out1, simSteps * IO_NETWORK_SIZE * sizeBytesFloat);
	max_ignore_kernel(actions1, "infoli0Kernel");
	max_ignore_lmem(actions1, "cpu2lmem");
	max_ignore_lmem(actions1, "Ini0");
	max_ignore_lmem(actions1, "iApp0");
	max_ignore_lmem(actions1, "IC0");
	max_ignore_lmem(actions1, "s0");


	printf("Actions Init.\n");
	actArray = max_mixed_actarray_init(maxfiles,numEngines);
	max_push_action(actArray, actions0);
	max_push_action(actArray, actions1);

	if (actArray == NULL)
		printf("Action array pointer Error.\n");



	printf("Running Output Read Phase on DFE.\n\n");
	max_run_array(engines, actArray);

	max_actarray_free(actArray);



	
//-----------------------------------------------------------------------------------------------------------------//


//Write output to file
	printf("Writing Output File\n");
	for (long i=0; i<simSteps ; i++){
		sprintf(temp, "%ld %.2f ", i+1, iApp0[i* IO_NETWORK_SIZE]);
	    	fputs(temp, pOutFile);
    		int cell = -1;
			for (long j=i*IO_NETWORK_SIZE; j<(i*IO_NETWORK_SIZE)+IO_NETWORK_SIZE; j++){
				cell++;
				sprintf(temp, " %d : %.8f",cell, Out0[j]);
				fputs(temp, pOutFile);
			}
		
			for (long j=i*IO_NETWORK_SIZE; j<(i*IO_NETWORK_SIZE)+IO_NETWORK_SIZE; j++){
				cell++;
				sprintf(temp, " %d : %.8f",cell, Out1[j]);
				fputs(temp, pOutFile);
			}

		sprintf(temp, "\n ");
    	fputs(temp, pOutFile);

	}


	fflush(stdout);
	//fclose (pOutFile);
	printf("Done.\n");
	return 0;
}
